"""App_base_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.urls import include, path
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from app.user.views import UserViewSet
from django.contrib.auth import views as auth_views
from app.groups.views import GroupViewSet
from app.passwordreset.views import PasswordResetViewSet, PasswordResetValidateViewSet

router = DefaultRouter()
router.register('users', UserViewSet, base_name='users')
router.register('groups', GroupViewSet, base_name='GroupViewSet')
router.register('password_reset', PasswordResetViewSet, base_name='password_reset')
router.register('password_reset_validate', PasswordResetValidateViewSet, base_name='password_reset_validate')

urlpatterns = [
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    # path('users/', include('app.user.urls')),
    path('api/login/', obtain_jwt_token, name='login'),
    path('api-auth/', include('rest_framework.urls')),
    # path('users/password/reset/', password_reset, {'html_email_template_name': 'registration/password_reset_email.html'},name='password_reset')
    # path('api/pass/users/password_reset', include('django_rest_passwordreset.urls', namespace='password_reset')),
    # path('auth/', include('pyctivex.urls'), namespace='apiauth')
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        #For django versions before 2.0:
         #url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns