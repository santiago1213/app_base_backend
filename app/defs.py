import socket
from datetime import datetime, timedelta

def hour_plus():
    return datetime.now() + timedelta(hours=1)