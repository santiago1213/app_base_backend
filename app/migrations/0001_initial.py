# Generated by Django 2.1.7 on 2019-04-04 16:15

import app.defs
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PasswordReset',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid1, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='fecha de creación')),
                ('expiration_date', models.DateTimeField(default=app.defs.hour_plus, verbose_name='fecha de expiracion')),
                ('ip', models.CharField(max_length=50, verbose_name='ip computador')),
                ('actived', models.BooleanField(default=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Usuario')),
            ],
        ),
    ]
