from django.conf import settings
from django.urls import include, path
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from app.user.views import UserViewSet
from django.contrib.auth import views as auth_views


router = DefaultRouter()
router.register('users', UserViewSet, base_name='users')



urlpatterns = [
    path('admin/', admin.site.urls),
    path('/', include(router.urls))
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        #For django versions before 2.0:
            #url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns