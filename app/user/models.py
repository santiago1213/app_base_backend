# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth import get_user_model

# Create your models here.
User = get_user_model()

LOGIN_APPLICATION = 'APPLICATION'
LOGIN_LDAP = 'LDAP'
