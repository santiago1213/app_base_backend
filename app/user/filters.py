'''
Filtros para el módulo de usuarios
UserFilter created by Andrés Felipe Blandón Palacio, Noviembre - 2018
'''
from django_filters import rest_framework as filters
from django.contrib.auth import get_user_model
User = get_user_model()


class UserFilter(filters.FilterSet):
    '''
    Filtro para User y UserData
    '''
    class Meta:
        model = User
        fields = {
            'id': ['in'],
            'document': ['icontains', 'exact'],
            'last_login': ['icontains', 'exact'],
            'first_name': ['icontains', 'exact'],
            'last_name': ['icontains', 'exact'],
            'username': ['icontains', 'exact'],
            'email': ['icontains', 'exact'],
            'is_active': ['exact'],
            'login_type': ['exact'],
            'groups': ['exact'],
            
        }
