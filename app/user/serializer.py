import requests
from django.conf import settings
from rest_framework.response import Response
from rest_framework import serializers
from django.contrib.auth import get_user_model
from app.groups.serializers import GroupSerializer

User = get_user_model()
        
class UserSerializer(serializers.ModelSerializer):
    '''
    User serializer
    '''
    url = serializers.HyperlinkedIdentityField(view_name='users-detail', read_only=True)
    password = serializers.CharField(label='Contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)

    def create(self, validated_data):
        groups_data = validated_data.pop('groups')
        # try:
            # validated_data.pop('confirm_password')
        # except:
            # pass

        user = User.objects.create_user(**validated_data)
        user.groups.set(groups_data)    
        return user


    class Meta:
        '''
        Meta tags
        '''

        model = User
        fields = (
            'id',
            'document',
            'username',
            'login_type',
            'email',
            'first_name',
            'last_name',
            'groups',
            'is_active',
            'is_superuser',
            'password',
            'url',
        )
        read_only_fields = ('url', )
        extra_kwargs = {'password': {'write_only': True}}


class UserSerializerList(serializers.ModelSerializer):
    '''
    User serializer
    '''
    url = serializers.HyperlinkedIdentityField(view_name='users-detail', read_only=True)
    password = serializers.CharField(label='Contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)

    groups = GroupSerializer(many=True)

    class Meta:
        '''
        Meta tags
        '''

        model = User
        fields = (
            'id',
            'document',
            'username',
            'login_type',
            'email',
            'first_name',
            'last_name',
            'groups',
            'is_active',
            'is_superuser',
            'password',
            'url',
        )
        read_only_fields = ('url', )
        extra_kwargs = {'password': {'write_only': True}}
