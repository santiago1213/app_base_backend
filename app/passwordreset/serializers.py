import requests
from datetime import datetime
from django.conf import settings
from rest_framework.response import Response
from rest_framework import serializers
from django.contrib.auth import get_user_model
from app.passwordreset.models import PasswordReset
from django.core.mail import send_mail
from django.core.exceptions import ObjectDoesNotExist
from app.user.models import LOGIN_APPLICATION
User = get_user_model()

class PasswordResetSerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name='password_reset-detail', read_only=True)
    email = serializers.EmailField(label='Correo electrónico', required=True, allow_null=True, allow_blank=True)
    
    def create(self, validated_data):
        try:
            user = User.objects.get(email=validated_data['email'])
            domain = self.context['request'].META['HTTP_ORIGIN']
            ip = self.context['request'].META['REMOTE_ADDR']

            if user:
                if user.login_type == LOGIN_APPLICATION:
                    passwordreset = PasswordReset.objects.create(ip=ip, user_id=user.id)
                    passwordreset.save()

                    if passwordreset: 

                        send_mail(
                            'RESTABLECER CONTRASEÑA',
                            'solicito un restablecimiento de contraseña',
                            'nnetflix155@gmail.com',
                            [validated_data['email']],
                            fail_silently=False,
                            html_message="<a href='{}/#/reset/password/{}/'>RESTABLECER LA CONTRASEÑA</a><br><p>En este link puede restablcer su contraseña.</p>".format(domain, passwordreset.id)
                        )
                        return user
                    else:
                        raise serializers.ValidationError({'detail':'Error al crear el token.'})
                else:
                    raise serializers.ValidationError({'email':'El usuario no es de APLICACION.'})
        
        except User.DoesNotExist:
            user = None 
        except ObjectDoesNotExist:
            user = None 
        raise serializers.ValidationError({'email': 'No existe un usuario registrado con este correo'})

    class Meta:
        model = PasswordReset
        fields = (
            'id',
            'date_created',
            'expiration_date',
            'ip',
            'actived',
            'user',
            'url',
            'email'
        )
        read_only_fields = ('url', 'date_created', 'expiration_date', 'ip', 'actived', 'user', )
        extra_kwargs = {'email': {'write_only': True}}

class PasswordResetSerializerList(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name='password_reset_validate-detail', read_only=True)
    password = serializers.CharField(label='Contraseña', required=True, allow_null=False, allow_blank=False, style={'input_type': 'password'}, write_only=True)
    confirm_password = serializers.CharField(label='Confirmar Contraseña', required=True, allow_null=False, allow_blank=False, style={'input_type': 'password'}, write_only=True)

    def update(self, instance, validated_data):
        if instance.actived == False:
            now = datetime.now()
            expiry_time = str(instance.expiration_date)
            expiry_time = expiry_time[0:26]
            expiry_time = datetime.strptime(expiry_time, '%Y-%m-%d %H:%M:%S.%f')
            if now < expiry_time:
                if validated_data['password'] == validated_data['confirm_password']:
                    try:
                        confirm_password = validated_data.pop('confirm_password')
                        user = User.objects.get(id=instance.user.id)
                        user.password = validated_data['password']
                        user.set_password(user.password)
                        user.save()

                        instance.actived = True
                        instance.save()
                        return user
                        
                    except Exception as e:
                        raise serializers.ValidationError({'detail': e})
                else:
                    raise serializers.ValidationError({'detail': 'hola'})
            else:
                raise serializers.ValidationError({'detail': 'hola2'})
        else:
            raise serializers.ValidationError({'detail': 'hol3'})

    class Meta:
        model = PasswordReset
        fields = (
            'id',
            'date_created',
            'expiration_date',
            'ip',
            'actived',
            'user',
            'url',
            'password',
            'confirm_password'
        )
        read_only_fields = ('url', 'date_created', 'expiration_date', 'ip', 'actived', 'user', )

