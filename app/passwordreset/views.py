from __future__ import unicode_literals

from datetime import datetime
from django.shortcuts import render
from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAdminUser


User = get_user_model()

from app.passwordreset.models import PasswordReset
from app.passwordreset.serializers import PasswordResetSerializer, PasswordResetSerializerList

class PasswordResetViewSet(viewsets.ModelViewSet):

    app_code = 'app'
    permission_code = 'passwordreset'
    serializer_class = PasswordResetSerializer
    queryset = PasswordReset.objects.all()
    permission_clase_by_action = {'create': [AllowAny], 'retrieve': [AllowAny], 'update': [AllowAny]}
    # filter_class = UserFilter
    # search_fields = ('username', 'first_name', 'last_name', 'email')
    # filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_clase_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]
    
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PasswordResetSerializerList
        elif self.request.method == 'PUT':
            return PasswordResetSerializerList
        return PasswordResetSerializer


class PasswordResetValidateViewSet(viewsets.ModelViewSet):
    
    serializer_class = PasswordResetSerializerList
    queryset = PasswordReset.objects.all()
    
    permission_clase_by_action = {'retrieve': [AllowAny], 'update': [AllowAny] }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_clase_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def retrieve(self, request, pk=None):
        passwordreset = PasswordReset.objects.get(id=pk)
        if passwordreset:
            now = datetime.now()
            expiry_time = str(passwordreset.expiration_date)
            expiry_time = expiry_time[0:26]
            expiry_time = datetime.strptime(expiry_time, '%Y-%m-%d %H:%M:%S.%f')
            if now < expiry_time:
                if passwordreset.actived == False:
                    return Response({'detail': 'pasaste.', 'codereset':'1324677y86876868776565543532234243255440'})                   
                else:
                    return Response({'detail': 'El link ya fue usado.', 'codereset':'1324677y86976868i7985543532034243255435'})
            else:
                return Response({'detail': 'El link ya expiró.', 'codereset':'13sad4607868768681765249035322342432554'})
        else:
            return Response({'detail': 'No token no existe.', 'codereset':'231324677y8687ds68776565543530000003255'})
    



