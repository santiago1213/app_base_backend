# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid

from django.db import models
from rest_framework.authtoken.models import Token
from django.contrib.auth import get_user_model

from app.defs import hour_plus
# Create your models here.
User = get_user_model()

class PasswordReset(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, null=False, verbose_name='fecha de creación')
    expiration_date = models.DateTimeField(null=False, verbose_name='fecha de expiracion', default=hour_plus)
    ip = models.CharField(max_length=50, verbose_name='ip computador', null=False)
    actived = models.BooleanField(default=False) 
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=False, verbose_name='Usuario')

    def __strs__(self):
        return self.ip