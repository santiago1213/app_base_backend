from rest_framework import viewsets
from app.groups.serializers import *

class GroupViewSet(viewsets.ModelViewSet):

    queryset = Group.objects.all()
    serializer_class = GroupSerializer